#include <stdio.h>
#include <windows.h>
#include <ctime>

/*
	Name: person database
	Copyright: All rights reserved
	Author: Martin Morh��
	Date: 17.11.17 15:05
	Description: 
*/
#define MAX_SIZE_MENO 20
#define MAX_SIZE_PRIEZVISKO 30

typedef struct datum{		// definicia struktury datum
	int rok;
	int den;
	int mesiac;
}Datum;

typedef struct osoba{		// definicia struktury osoba
	char meno[MAX_SIZE_MENO];
	char priezvisko[MAX_SIZE_PRIEZVISKO];
	Datum narodenie;
}Osoba;

typedef struct zoznamosob{	//definicia struktury zoznamosob
	Osoba zoznam[10];
	int pocetosob;
}Zoznam;

int terminal_select=0;	//nastavenie a vytvorenie glob�lnej premennej vybrania mo�nosti termin�lu
Zoznam zoznamOsob;

void init_zoznam(Zoznam* zoz){	//funkcia na inicializ�ciu
	zoz->pocetosob=0;
}
void narodeniny(){
	time_t now = time(0);
	tm *ltm = localtime(&now);
	unsigned int year = ltm->tm_year;
	char mesiac = 1 + ltm->tm_mon;
	char den = ltm->tm_mday;
	char meno_a_priezvisko[(MAX_SIZE_MENO + MAX_SIZE_PRIEZVISKO + 40)*zoznamOsob.pocetosob];
	int pocetmeniny = 0;
	int pocetmenoapriezvisko = sizeof(meno_a_priezvisko); ;
	int akt_meniny = 0;
	for (int i = 0;pocetmenoapriezvisko >= i;i++){
		meno_a_priezvisko[i] = 0;
	}
	for(int i = 0;i < zoznamOsob.pocetosob;i++){
		if(zoznamOsob.zoznam[i].narodenie.den == den){	//den
			if(zoznamOsob.zoznam[i].narodenie.mesiac == mesiac){	//mesiac
			pocetmeniny++;
			}	
		}
		}
	for(int i = 0;i < zoznamOsob.pocetosob;i++){
		if(zoznamOsob.zoznam[i].narodenie.den == den){	//den
			if(zoznamOsob.zoznam[i].narodenie.mesiac == mesiac){	//mesiac;	
			system("title person database - narodeniny");
			akt_meniny++;
				//vytvorenie pola
			if(akt_meniny == pocetmeniny){
			sprintf(meno_a_priezvisko,"%s %s %s",meno_a_priezvisko,zoznamOsob.zoznam[i].meno,zoznamOsob.zoznam[i].priezvisko);	
			}
			else{
				if(akt_meniny == pocetmeniny-1){
					sprintf(meno_a_priezvisko,"%s %s %s a",meno_a_priezvisko,zoznamOsob.zoznam[i].meno,zoznamOsob.zoznam[i].priezvisko);
				}
				else{
					sprintf(meno_a_priezvisko,"%s %s %s,",meno_a_priezvisko,zoznamOsob.zoznam[i].meno,zoznamOsob.zoznam[i].priezvisko);
				}
			}
			}	
		}
		}
		if(pocetmeniny){
			if(pocetmeniny > 1){
			sprintf(meno_a_priezvisko,"%s maj� narodeniny",meno_a_priezvisko);	
			}
			else{
				sprintf(meno_a_priezvisko,"%s m� narodeniny",meno_a_priezvisko);
			}
		MessageBox(NULL,meno_a_priezvisko,"person database",MB_OK | MB_ICONINFORMATION);
	}
}
void vypismenu(){
	system("cls");	//vymazanie termin�lu
	printf("Akcie:\n");	//vyp�sanie mo�nost� termin�lu
	printf("1)pridat osobu\n");	//vyp�sanie mo�nost� termin�lu
	printf("2)zmazat osobu\n");	//vyp�sanie mo�nost� termin�lu
	printf("3)upravit osobu\n");	//vyp�sanie mo�nost� termin�lu
	printf("4)vypis vsetky osoby\n");	//vyp�sanie mo�nost� termin�lu
	printf("5)vypis jednu osobu\n");	//vyp�sanie mo�nost� termin�lu
	printf("6)ulozit\n");	//ulozit
	printf("7)nacitat\n");	//nacitat
	printf("8)upravit farbu terminalu\n");
	printf("9)info\n");
	printf("10)exit\n");	//vyp�sanie mo�nost� termin�lu
	printf("Napiste cislo: ");	//vyp�sanie mo�nost� termin�lu
	scanf("%d",&terminal_select);	//
	getchar();
	printf("\n");
}
void vypis_vsetky_osoby(){
	printf("osoby:\n");
	if(zoznamOsob.pocetosob <= 0){
		printf("Ziadna\n");
	}
	else{
			for(int i = 0;i < zoznamOsob.pocetosob;i ++){
				printf("%d)%s %s\n",(i + 1),zoznamOsob.zoznam[i].meno,zoznamOsob.zoznam[i].priezvisko);
			}
		}
}
void pridanie_osoby(){
	if(zoznamOsob.pocetosob > 9){
	MessageBox(NULL,"bol prekro�en� maxim�lny po�et os�b (10)","person database",MB_OK | MB_ICONERROR);
	}
	else{

		printf("meno osoby: ");
		scanf("%s",&zoznamOsob.zoznam[zoznamOsob.pocetosob].meno);
		printf("priezvisko osoby: ");
		scanf("%s",&zoznamOsob.zoznam[zoznamOsob.pocetosob].priezvisko);
		printf("napiste informacie o narodeni osoby\n");
		printf("rok: ");
		scanf("%d",&zoznamOsob.zoznam[zoznamOsob.pocetosob].narodenie.rok);
		printf("den: ");
		scanf("%d",&zoznamOsob.zoznam[zoznamOsob.pocetosob].narodenie.den);
		printf("mesiac: ");
		scanf("%d",&zoznamOsob.zoznam[zoznamOsob.pocetosob].narodenie.mesiac);
		zoznamOsob.pocetosob ++;
		}
	}
void zmena_osoby(){
	int osoba;
	int zmenit = 0;
	if(zoznamOsob.pocetosob <= 0){
		MessageBox(NULL,"Po�et os�b je nula","person database",MB_OK | MB_ICONERROR);
	}

	
	else{
		vypis_vsetky_osoby();
		printf("osoba: ");
		scanf("%d",&osoba);
		printf("\n");
		if(zoznamOsob.pocetosob < osoba){
		MessageBox(NULL,"neplatn� osoba","person database",MB_OK | MB_ICONERROR);
		}
		printf("zmenit:\n");
		printf("1)meno\n");
		printf("2)priezvisko\n");
		printf("3)narodenie\n");
		scanf("%d",&zmenit);
		printf("\n");
		switch(zmenit){
		case 1:
		printf("meno osoby: \n");
		scanf("%s\n",&zoznamOsob.zoznam[osoba-1].meno);
		break;
		case 2:
		printf("priezvisko osoby: \n");
		scanf("%s",&zoznamOsob.zoznam[osoba-1].priezvisko);
		printf("\n");
		break;
		case 3:
			printf("napiste informacie o narodeni osoby\n");
		printf("rok: \n");
		scanf("%d",&zoznamOsob.zoznam[osoba-1].narodenie.rok);
		printf("\n");
		printf("den: \n");
		scanf("%d",&zoznamOsob.zoznam[osoba-1].narodenie.rok);
		printf("\n");
		printf("mesiac: \n");
		scanf("%d",&zoznamOsob.zoznam[osoba-1].narodenie.rok);
		printf("\n");
		break;
		default:
			MessageBox(NULL,"chyba","person database",MB_OK | MB_ICONERROR);
		break;
		}
		}
}

int vypisanie_osoby(){
		vypis_vsetky_osoby();
		if(zoznamOsob.pocetosob){
		int osobavypisat = 0;
		printf("osoba: ");
		scanf("%d",&osobavypisat);
		osobavypisat --;
		if(zoznamOsob.pocetosob > osobavypisat){
		printf("%s %s datum narodenia: rok: %d den: %d mesiac: %d\n",
			zoznamOsob.zoznam[osobavypisat].meno,
			zoznamOsob.zoznam[osobavypisat].priezvisko,
			zoznamOsob.zoznam[osobavypisat].narodenie.rok,
			zoznamOsob.zoznam[osobavypisat].narodenie.den,
			zoznamOsob.zoznam[osobavypisat].narodenie.mesiac
			);
		}
		else{
			MessageBox(NULL,"chyba!!!!","person database",MB_OK | MB_ICONERROR);
		}
		}
		system("pause");
		}
	
void delete_person(){
	int osoba;
	vypis_vsetky_osoby();
	scanf("%d",&osoba);
	if(MessageBox(NULL,"Nazozaj chcete vymazat tuto osobu","person database",MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)== IDYES){
	for(int i = osoba;i < (zoznamOsob.pocetosob);i++){
		zoznamOsob.zoznam[i -1] = zoznamOsob.zoznam[i];
	}
	zoznamOsob.pocetosob --;
	MessageBox(NULL,"osoba zmazan�","person database",MB_OK | MB_ICONWARNING);
	}
	
	
}
char uprava_farby (){
	
	char koncitno = 0;
	char pozadie = 5;
	char text = 32;
	char color[10];
	do{											//pozadie
	koncitno = 0;
	printf("0 = Cierna   8 = Siva\n");
	printf("1 = Modra    9 = Svetlo modra\n");
	printf("2 = Zelena   A = Svetlo zelena\n");
	printf("3 = Vodna    B = Svetlo vodna\n");
	printf("4 = Cervena  C = Svetlo cervena\n");
	printf("5 = Ruzova   D = Svetlo ruzova\n");
	printf("6 = Zlta     E = Svetlo zlta\n");
	printf("7 = Biela    F = Tmavo biela\n");
	printf("Z = Zavriet zmenu farby\n");
	printf("Pozadie:");
	scanf("%c",&pozadie);
	printf("\n");
	getchar();
		//if(!((pozadie > -1 && pozadie < 8) || (pozadie > 64 && pozadie < 71) || (pozadie > 96 && pozadie < 103) || (pozadie == 90))){
		if((pozadie < '0' || pozadie > '7') && 
			(pozadie < 'A' || pozadie > 'F') && 
			(pozadie != 'Z') &&
			(pozadie < 'a' || pozadie > 'f') && 
			(pozadie != 'z')){
			MessageBox(NULL,"Neplatn� znak pre pozadie","person database",MB_OK | MB_ICONERROR);
		}
	system("cls");
	}while(koncitno);
								//text
	do{
	koncitno = 0;
	printf("0 = Cierna   8 = Siva\n");
	printf("1 = Modra    9 = Svetlo modra\n");
	printf("2 = Zelena   A = Svetlo zelena\n");
	printf("3 = Vodna    B = Svetlo vodna\n");
	printf("4 = Cervena  C = Svetlo cervena\n");
	printf("5 = Ruzova   D = Svetlo ruzova\n");
	printf("6 = Zlta     E = Svetlo zlta\n");
	printf("7 = Biela    F = Tmavo biela\n");
	printf("Z = Zavriet zmenu farby\n");
	printf("Text:");
	scanf("%c",&text);
	printf("\n");
	getchar();
		switch (text){
				case 'z':
		if(MessageBox(NULL,"Nazozaj chcete ukon�i� zmenu farby","person database",MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)== IDYES){
			return 0;
		}
		else{
			koncitno = 1;
		}
		break;	
				case 'Z':
		if(MessageBox(NULL,"Nazozaj chcete ukon�i� zmenu farby","person database",MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)== IDYES){
			return 0;
		}
		else{
			koncitno = 1;
		}
		break;	
				case 'a':
			
		break;	
				case 'b':
			
		break;	
				case 'c':
			
		break;	
				case 'd':
			
		break;	
				case 'e':
			
		break;	
				case 'f': //velke
			
		break;	
				case 'A':
			
		break;	
				case 'B':
			
		break;	
				case 'C':
			
		break;	
				case 'D':
			
		break;	
				case 'E':
			
		break;	
				case 'F':
			
		break;	
				case '0':
			
		break;	
				case '1':
			
		break;	
				case '2':
			
		break;	
				case '3':
			
		break;	
				case '4':
			
		break;	
				case '5':
			
		break;	
				case '6':
			
		break;	
				case '7':
			
		break;	
				case '8':
			
		break;	
				case '9':
			
		break;	
				default:
			MessageBox(NULL,"Neplatn� znak pre text","person database",MB_OK | MB_ICONERROR);
			return 1;
		break;	
	}
	system("cls");
	}while(koncitno);
	sprintf(color, "color %c%c", pozadie, text);
	system(color);
}
void save(){
	FILE *file = fopen("save.perdat","w");
	if(file == NULL){
		MessageBox(NULL,"S�bor save.perdat sa nopadarilo otvori�","person database",MB_OK | MB_ICONERROR);
		return;
	}
	fprintf(file,"%d\n",zoznamOsob.pocetosob);
	for(int i = 0;i < zoznamOsob.pocetosob;i++){
		fprintf(file,"%s\n%s\n%d\n%d\n%d\n",zoznamOsob.zoznam[i].meno,
		zoznamOsob.zoznam[i].priezvisko,
		zoznamOsob.zoznam[i].narodenie.den,
		zoznamOsob.zoznam[i].narodenie.mesiac,
		zoznamOsob.zoznam[i].narodenie.rok);
	}
	fclose(file);
}
void load(){
	FILE *file = fopen("save.perdat","r");
	if(file == NULL){
	MessageBox(NULL,"S�bor save.perdat sa nopadarilo otvori�","person database",MB_OK | MB_ICONERROR);
	return;
	}
	fscanf(file,"%d\n",&zoznamOsob.pocetosob);
	for(int l = 0;l < zoznamOsob.pocetosob;l++)	{
		fscanf(file,"%s\n",&zoznamOsob.zoznam[l].meno);
		fscanf(file,"%s\n",&zoznamOsob.zoznam[l].priezvisko);
		fscanf(file,"%d\n",&zoznamOsob.zoznam[l].narodenie.den);
		fscanf(file,"%d\n",&zoznamOsob.zoznam[l].narodenie.mesiac);
		fscanf(file,"%d\n",&zoznamOsob.zoznam[l].narodenie.rok);
	}
	fclose(file);
}
int main () {
	system("title person database");
	init_zoznam(&zoznamOsob);
	load();
	narodeniny();
	while(1){
		system("title person database - menu");
		
		vypismenu();
		
		switch (terminal_select){
			case 1:
				system("cls");
				if(zoznamOsob.pocetosob > 9){
					MessageBox(NULL,"bol prekro�en� maxim�lny po�et os�b (10)","person database",MB_OK | MB_ICONERROR);
				}
				else{
					system("title person database - pridanie osoby");
					pridanie_osoby();
					system("title person database - menu");
				}
			break;
		
			case 2:
				system("cls");
			if(zoznamOsob.pocetosob <= 0){
				MessageBox(NULL,"chyba 0 os�b","person database",MB_OK | MB_ICONERROR);
			}	
			else{
				system("title person database - vymazat osobu");
				delete_person();
				system("title person database - menu");
			}
			break;
		
			case 3:
			system("title person database - upravenie osoby");
			system("cls");
			//MessageBox(NULL,"podporene iba v platenej verzii","person database",MB_OK | MB_ICONINFORMATION);
			/*printf("podporene iba v platenej verzii");
			system("pause");
			system("cls");*/
			/*zmena_osoby();
			system("pause");*/
			zmena_osoby();
			system("title person database - menu");
			break;
		
			case 4:
				system("title person database - vypisanie vsetkych osob");
				system("cls");
				vypis_vsetky_osoby();
				system("pause");
				system("title person database - menu");
			break;	
			case 5:
				system("title person database - vypisanie osoby");
				system("cls");
				vypisanie_osoby();
				system("title person database - menu");
			break;
			
			case 6:
			system("cls");
			save();
			break;
			
			case 7:
			system("cls");
			//MessageBox(NULL,"podporene iba v platenej verzii","person database",MB_OK | MB_ICONINFORMATION);
			load();
			break;
			
			case 8:
				system("title person database - uprava farby");
				system("cls");
				//MessageBox(NULL,"podporene iba v platenej verzii","person database",MB_OK | MB_ICONINFORMATION);
				/*printf("podporene iba v platenej verzii");
				system("pause");
				system("cls");*/
				while(uprava_farby()){
					system("cls");
				}
				system("title person database - menu");
			break;
			case 9:
				system("title person database - info");
				system("cls");
				printf("info:\n");
				printf("Created by Martin Morhac\n");
				printf("All rights reserved\n");
				system("pause");
				system("cls");
				system("title person database - menu");
			break;
			case 10:
				system("title person database - vypnutie");
				system("cls");
				if(MessageBox(NULL,"chcete ulo�i� v�etky zmeny","person database",MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON1)== IDYES){
					save();
				}
				MessageBox(NULL,"dovidenia","person database",MB_OK);
				system("taskkill /im persondatabase.exe");
				return 0;
			break;
			default:
				
				
				MessageBox(NULL,"chybne cislo akcie","person database",MB_OK | MB_ICONERROR);
			break;
		}
	}
}
